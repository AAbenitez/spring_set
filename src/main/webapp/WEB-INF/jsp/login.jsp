<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link href="<c:url value="css/app.css" />" rel="stylesheet"
	type="text/css">
<title>Spring Security E-set Login</title>
</head>
<body class="security-app">

	<div class="details">
		<!--  img alt="Imagen Marangatu" src="<c:url value='/assets/images/marangatu.gif' />"-->
		<!--  img alt="Imagen Set" src="<c:url value='/assets/images/set.gif' />" -->
		<h2>Conexión al Sistema E-SET</h2>		
	</div>

	<form action="/login" method="post">

		<div class="lc-block">
			<div>
				<input type="text" class="style-4" name="username"
					placeholder="Nombre del Usuario" />
			</div>
			<div>
				<input type="password" class="style-4" name="password"
					placeholder="Contraseña" />
			</div>
			<div>
				<input type="submit" value="Iniciar Sesión" class="button red small" />
			</div>
			<c:if test="${param.error ne null}">
				<div class="alert-danger">Usuario o contraseña erronea.</div>
			</c:if>
			<c:if test="${param.logout ne null}">
				<div class="alert-normal">Has cerrado sesión.</div>
			</c:if>
		</div>
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>

</body>
</html>